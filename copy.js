"use strict";

function elem(id) { return document.getElementById(id); }

window.addEventListener('DOMContentLoaded', function() {
    elem('copySelectionInput').addEventListener( 'click', function(e) {
        if( true ) {
            //var selectionElement= elem('selectionLink');
            var selectionElement= elem('selectionInput');

            var range = document.createRange();
            range.selectNode(selectionElement);
            //range.setStartBefore(selectionElement);
            //range.setEndAfter(selectionElement);

            var sel = window.getSelection();
            sel.removeAllRanges();
            sel.addRange(range);
        }
        else {
            elem('selectionInput').select();
        }
        document.execCommand("copy");
        return false;
    });  
});